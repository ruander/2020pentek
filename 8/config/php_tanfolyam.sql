-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2021. Feb 26. 15:25
-- Kiszolgáló verziója: 10.4.17-MariaDB
-- PHP verzió: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `php_tanfolyam`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `articles`
--

CREATE TABLE `articles` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `seo_title` varchar(255) NOT NULL,
  `lead` varchar(400) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `author` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `time_published` datetime DEFAULT NULL,
  `time_created` datetime NOT NULL,
  `time_updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `articles`
--

INSERT INTO `articles` (`id`, `title`, `seo_title`, `lead`, `content`, `image`, `author`, `status`, `time_published`, `time_created`, `time_updated`) VALUES
(1, 'Teszt cikk', 'teszt-cikk', 'Teszt bevezető', 'Teszt tartalom...', NULL, 'testuser', 1, '2021-02-26 14:35:45', '2021-02-26 14:35:45', NULL),
(3, 'árvíztűrő tükörfúrógép ?$123_-.;', 'arvizturo-tukorfurogep---123_', '', '', NULL, '', 0, NULL, '2021-02-26 15:17:24', NULL),
(4, 'teszt cím 2', 'teszt-cim-2', '', '', NULL, '', 0, NULL, '2021-02-26 15:17:54', NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `sessions`
--

CREATE TABLE `sessions` (
  `sid` varchar(64) NOT NULL,
  `spass` varchar(64) NOT NULL,
  `stime` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `sessions`
--

INSERT INTO `sessions` (`sid`, `spass`, `stime`) VALUES
('uhkvp2nv4g1qhbi39176dj2ad5', '02f0de4b2e5561cf59a00e1137a3be36', 1614349074);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(64) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0-inaktív, 1-aktív',
  `time_created` datetime NOT NULL DEFAULT current_timestamp(),
  `time_updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `status`, `time_created`, `time_updated`) VALUES
(1, 'testuser', 'hgy@iworkshop.hu', '$2y$10$6EDzrdEjR63knEDxZcT1HOrSatxYxObdEewUNXjcLekE/RFI9XEI6', 1, '2021-02-12 14:19:26', NULL),
(2, 'hgy', 'hgy@ruander.hu', '$2y$10$ZhPBSrFc5LOaRq4oB94Y5O2od.mZD76mRiv496j1S4wf2jG9x6bFS', 1, '2021-02-12 14:53:48', NULL),
(3, 'gdfhh', 'teszt2@elek.hu', '$2y$10$gOZXXYi305pCdWrlpt0.dOfYsbp1o3XrIb3/HBe4W4L/EoWIU3X/u', 0, '2021-02-26 09:22:22', '2021-02-26 10:18:04'),
(4, 'teszt admin', 't@t.t2', '$2y$10$6o7SNp6DPNdYxnGlcgLMIejPijZNzBndD1Qv/DHz/Do1NDesqItZm', 0, '2021-02-26 10:30:08', '2021-02-26 10:31:06'),
(6, 'ccc', 'd@d.d', '$2y$10$xtgPf7hJEki3LoSEPT.rVeEp5OFam01v5eh0al0jSCay023xWY4qq', 1, '2021-02-26 13:35:30', NULL);

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `seo_title` (`seo_title`);

--
-- A tábla indexei `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`sid`);

--
-- A tábla indexei `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT a táblához `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
