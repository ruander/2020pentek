<?php
//Saját eljárások gyüjteménye

/**
 * @param $str
 * @return string
 */
function ekezettelenit($str) {
    $bad  = array('á','ä','é','í','ó','ö','ő','ú','ü','ű','Á','Ä','É','Í','Ó','Ö','Ő','Ú','Ü','Ű',' ');
    $good = array('a','a','e','i','o','o','o','u','u','u','a','a','e','i','o','o','o','u','u','u','-');
    $str = mb_strtolower($str,"utf-8");//kisbetűsre
    $str = str_replace($bad, $good, $str);//karakterek cseréje
    $str = preg_replace("/[^A-Za-z0-9_]/", "-", $str);//maradék eltávolítása
    $str = rtrim($str,'-');//végződő - eltávolítása
    return $str;
}

/**
 * @param $str
 * @return string
 */
function ekezettelenitFileName($str) {
    $bad  = array('á','ä','é','í','ó','ö','ő','ú','ü','ű','Á','Ä','É','Í','Ó','Ö','Ő','Ú','Ü','Ű',' ');
    $good = array('a','a','e','i','o','o','o','u','u','u','a','a','e','i','o','o','o','u','u','u','-');
    $str = mb_strtolower($str,"utf-8");//kisbetűsre
    $str = str_replace($bad, $good, $str);//karakterek cseréje
    $str = preg_replace("/[^A-Za-z0-9_.]/", "-", $str);//maradék eltávolítása
    $str = rtrim($str,'-');//végződő - eltávolítása
    return $str;
}

/**
 * Hibakiíró eljárás az input mezőkhöz
 * @param $fieldName
 * @return string
 */
function getError($fieldName)
{
    global $hiba;//az eljárás idejéig a hiba változó globális így 'látja' az eljárásunk
    return isset($hiba[$fieldName]) ? $hiba[$fieldName] : '';
}


/**
 * Input mezők value értékeit adja vissza ha ki volt töltve
 * @param $fieldName | mező name tulajdonsága ... name="fieldname"
 * @param array $dataRow | az adatbázis lekérés kibontot asszociativ tömbje (mezőnév = db fieldname !)
 * @return mixed
 */
function getValue($fieldName, $dataRow = [])
{
    $ret = false;
    if(filter_input(INPUT_POST, $fieldName)){//ha postban van adat az az elsődleges
        $ret = filter_input(INPUT_POST, $fieldName);
    }elseif(array_key_exists($fieldName, $dataRow)){//adatbázisból ha van adat...
        $ret = $dataRow[$fieldName];
    }

    return $ret;
}

/**
 * Kiléptetés
 * @return void
 */
function logout(){
    global $link;
    //db bejegyzés törlése
    mysqli_query($link,"DELETE FROM sessions WHERE sid = '".session_id()."' LIMIT 1") or die(mysqli_error($link));
    //mf roncsolása
    $_SESSION = null;
    session_destroy();
}

/**
 * Érvényes belépés ellenőrzése
 * @return bool
 */
function auth()
{
    global $link;

    $sid = session_id();
    $spass = null;
    if (isset($_SESSION['userdata'])) {
        $spass = md5($_SESSION['userdata']['id'] . $sid . SECRET_KEY);
    }
    $now = time();
    $expired = $now - (15 * 60);//most 15 perc
    //takarítás (lejárt munkafolyamatok)
    mysqli_query($link, "DELETE FROM sessions WHERE stime < $expired");
    //nézzük meg találunk e érvényes login eredményt a sessionsben
    $qry = "SELECT spass FROM sessions WHERE sid = '$sid' LIMIT 1";
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));
    $row = mysqli_fetch_row($result);
    //var_dump($row);
    if (isset($row) && $row[0] === $spass) {
        //stime update
        mysqli_query($link, "UPDATE sessions SET stime = $now WHERE sid = '$sid' LIMIT 1") or die(mysqli_error($link));
        return true;
    }
    return false;
}

/**
 * Beléptető eljárás
 * @return bool
 */
function login(){
    global $link;//az eljárás látni fogja

    $email = mysqli_real_escape_string($link,filter_input(INPUT_POST, 'email'));

    //jelszó lekérése az emailhez
    $qry = "SELECT id,name,email,password FROM users WHERE email = '$email' AND status = 1 LIMIT 1";
    $result = mysqli_query($link,$qry) or die(mysqli_error($link));
    $row = mysqli_fetch_assoc($result);
    //var_dump($row);
    if(!$row) return false;//ha nincs row, pápá
    $password = filter_input(INPUT_POST, 'password');
    //jelszó ellenőrzése
    $passCheck = password_verify($password,$row['password']);
    if($passCheck) {
        //azomositó a mf azonosító lesz, a jelszó több darabból áll
        $sid = $_SESSION['id'] = session_id();
        $spass = md5($row['id'].$sid.SECRET_KEY);
        $stime = time();//UNIX timestamp
        //var_dump('<pre>',$sid,$spass,$stime);
        unset($row['password']);
        $_SESSION['userdata']=$row;
        //töröljük az esetleges bertagadt munkafolyamatot
        mysqli_query($link,"DELETE FROM sessions WHERE sid = '$sid'");
        //eltároljuk a belépést a sessions táblában
        $qry = "INSERT INTO sessions(sid,spass,stime) VALUES('$sid','$spass',$stime)";
        mysqli_query($link,$qry) or die(mysqli_error($link));
        //sikeres belépés...
        return true;
    }
    return false;
}