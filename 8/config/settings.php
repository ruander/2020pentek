<?php
//beállítások
const SECRET_KEY = 's3cR3t_K3Y!';//titkosító kulcs
const MODULE_DIR = 'modules/';//modulok mappája
const MODULE_EXT = '.php';//modulfileok kiterjesztése (akár .inc is lehetne)

//admin menüpontok
const ADMIN_MENU = [
    0 => [
        'title' => 'vezérlőpult',
        'module' => 'dashboard',
        'icon' => 'fa fa-dashboard'
    ],
    1 => [
        'title' => 'cikkek',
        'module' => 'articles',
        'icon' => 'fa fa-some-icon-to-select'
    ],

    10 => [
        'title' => 'felhasználók',
        'module' => 'users',
        'icon' => 'fa fa-user'
    ],
];