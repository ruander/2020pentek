<?php
require "../config/connect.php";//db csatlakozás betöltése
require "../config/settings.php";//beállítások
require "../config/functions.php";//saját eljárások
//Munkafolyamatok:
session_start();
if (auth()) {//ha tudunk azonosítani, mit keresünk itt? irány az index
    header('location:index.php');
    exit();
}
$info = "Kérjük írja be a bejelentkezési adatokat:";
//var_dump($_SESSION, session_id());
if (!empty($_POST)) {
    if (login()) {
        //irányítsunk át az indexre
        header('location:index.php');
        exit();
    } else {
        $info = 'Nem megfelelő email/jelszó páros';
    }
}
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Belépés - Adminfelület</title>
</head>
<body>
<form method="post">
    <h2>Bejelentkezés</h2>
    <div class="info">
        <?php echo $info; ?>
    </div>
    <label>Email
        <input type="text" name="email" value="<?php echo filter_input(INPUT_POST, 'email') ?>"
               placeholder="email@cim.hu"></label>
    <label>
        Jelszó
        <input type="password" name="password" value="" placeholder="******">
    </label>
    <button>Belépés</button>
</form>
</body>
</html>
