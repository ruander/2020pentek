<?php
//require "../config/connect.php";//adatbázis kapcsolat betöltése
//require "../config/functions.php";//saját eljárások betöltése
if(!$link){
    header('location:index.php');
    exit();
}
//@todo HF: vizsga kiváltó feladat -> articles teljes funkcionalítással képfeltöltés nélkül (ott legyen csak egy file input)
//@figyelj!! -> published legyen datetime-local tipusu naptár input
//felhasználok kezelése lesz majd (CRUD)
$action = filter_input(INPUT_GET, 'action');
$tid = filter_input(INPUT_GET, 'tid', FILTER_VALIDATE_INT) ?: null;
$dbTable = 'articles';//ebbe a táblába dolgozik a modul
//$output = '';//itt lesznek a kiírandó elemek

if (!empty($_POST)) {
    //var_dump('<pre>',$_POST);
    //hibakezelés
    $hiba = [];//üres hibatömb, ide gyűjtjük a hibákat
    //...hibák kezelése, tárolása
    //title kötelező, seo_title a titleből átalakaítva 'slug'gá
    $title = strip_tags(mysqli_real_escape_string($link,filter_input(INPUT_POST,'title')));//adat kiszűrése védelmekkel
    if(!$title){
        $hiba['title'] = '<span class="error">Kötelező címet adni!</span>';
    }else{
        //átalakítjuk sluggá
        $seo_title = ekezettelenit($title);
    }

    //Státusz
    $status = filter_input(INPUT_POST, 'status')?:0;


    if (empty($hiba)) {
        //adatok tisztázása
        $data = [
            'title' => $title,
            'seo_title' => $seo_title,
            'status' => $status
        ];

        if($action == 'create') {
            $data['time_created'] = date('Y-m-d H:i:s');
            //echo '<pre>' . var_export($data, true) . '</pre>';
            //beírás adatbázisba
            $qry = "INSERT INTO 
                        `$dbTable` ( 
                                 `".implode("`,`",array_keys($data))."`) 
                        VALUES ( 
                                '".implode("','",$data)."');";
        }else{
            //módosítás
            $data['time_updated'] = date('Y-m-d H:i:s');

            $qry = "UPDATE `$dbTable` SET ";
            $set = [];//SETek tömbje
            foreach($data AS $k => $v){//SET ek
                $set[]= " `$k` = '$v' ";
            }
            $qry .= implode(',',$set);//setek befűzése a querybe
            //záradék
            $qry .= " WHERE id = $tid LIMIT 1";

        }

        mysqli_query($link, $qry) or die(mysqli_error($link));//kérés futtatása
        //átirányítunk a listázásra
        header('location:' . $baseURL);
        exit();
    }
}


switch ($action) {
    case 'delete':
        //ha van id, törlünk
        if ($tid) {
            mysqli_query($link, "DELETE FROM $dbTable WHERE id = $tid LIMIT 1") or die(mysqli_error($link));
        }
        header('location:' . $baseURL);//irányítás a listára
        exit();
        break;

    case 'update':
        $output .= 'módosítunk(id)(kitöltött form ˇˇˇˇ)';
        if($tid){//user adatok lekérése
            $qry = "SELECT status FROM $dbTable WHERE id = $tid LIMIT 1";
            $result = mysqli_query($link, $qry) or die(mysqli_error($link));
            $row = mysqli_fetch_assoc($result);
            echo '<pre>' . var_export($row, true) . '</pre>';
        }

        if($row){
            //módosítás űrlap
            $form = '<form method="post">
    <h2>Regisztrációs adatlap</h2>';
//név
            $form .= '<label>
        <span>Név<sup>*</sup></span>
        <input type="text" name="name" placeholder="Gipsz Jakab"
               value="' . getValue('name', $row) . '">' . getError('name') . '</label>';

//email
            $form .= '<label>
        <span>Email<sup>*</sup></span>
        <input type="text" name="email" placeholder="email@cim.hu"
               value="' . getValue('email', $row) . '">' . getError('email') . '</label>';

//Jelszó
            $form .= '<label>
        <span>Jelszó<sup>*</sup></span>
        <input type="password" name="password" placeholder="******" value="">' . getError('password') . '</label>';

//Jelszó újra
            $form .= '<label>
        <span>Jelszó újra<sup>*</sup></span>
        <input type="password" name="repassword" placeholder="******" value="">' . getError('repassword') . '</label>';

            //Státusz @todo: HF státusz működés ellenőrzése / megvalósítása (hibás űrlap esetén is teszteld!)
            $checked = filter_input(INPUT_POST, 'status') ? 'checked' : '';//volt-e kipipálva?
            $form .= '<label><span>';
            $form .= '<input type="checkbox" name="status" value="1" ' . $checked . '>
         Státusz</span></label>';
            $form .= '<button>Felhasználó módosítása</button>
        </form>';

            $output .= $form;
        }
        break;

    case 'create':
        $output .= 'új elemet viszünk fel (üres form)';
        $formTitle = '<h2>Új cikk felvitele</h2>';//űrlap címe lesz
        //új űrlap
        $form = '<form method="post">';
        $form .= $formTitle;
//cím
        $form .= '<label>
        <span>Cím<sup>*</sup></span>
        <input type="text" name="title" placeholder="Szuper cikk"
               value="' . getValue('title') . '">' . getError('title') . '</label>';
//seo cím (slug)
        $form .= '<label>
        <span>Seo cím</span>
        <input type="text" name="seo_title" placeholder="szuper-cikk"
               value="' . ( isset($seo_title) ? $seo_title : '' ) . '" readonly></label>';

        //Státusz
        $checked = filter_input(INPUT_POST, 'status') ? 'checked' : '';//volt-e kipipálva?
        $form .= '<label><span>';
        $form .= '<input type="checkbox" name="status" value="1" ' . $checked . '>
         Státusz</span></label>';
        $formButton = 'Új cikk felvitele';//gombszöveg
        $form .= '<button>'.$formButton.'</button></form>';

        $output .= $form;
        break;

    default:
        $table = '';

        //felhasználó lista
        $qry = "SELECT id, title, author, status FROM $dbTable";//lekérés összeösszeállítása
        $results = mysqli_query($link, $qry) or die(mysqli_error($link));
        //új felvitel opció
        $table .= '<a href="'.$baseURL.'&amp;action=create">Új felvitel</a>';
        //táblázat felépítése
        $table .= '<table border="1">';
        //fejléc
        $table .= '<tr>
                        <th>id</th>
                        <th>cím</th>
                        <th>szerző</th>
                        <th>status</th>
                        <th>művelet</th>
                    </tr>';
        //sorok
        while ($row = mysqli_fetch_assoc($results)) {
            $table .= '<tr class="data-row">
                            <td>' . $row["id"] . '</td>
                            <td>' . $row["title"] . '</td>
                            <td>' . $row["author"] . '</td>
                            <td>' . $row["status"] . '</td>
                            <td><a class="btn btn-warning btn-update" href="'.$baseURL.'&amp;action=update&amp;tid=' . $row["id"] . '">módosít</a> | <a onclick="return confirm(\'Biztosan törlöd?\');"  class="btn btn-danger btn-delete" href="'.$baseURL.'&amp;action=delete&amp;tid=' . $row["id"] . '">törlés</a></td>
                        </tr>';
        }
        $table .= '</table>';
        $output .= $table;
        break;
}

//echo $output; //index vette át a kiírását
