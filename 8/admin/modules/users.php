<?php
//require "../config/connect.php";//adatbázis kapcsolat betöltése
//require "../config/functions.php";//saját eljárások betöltése
if(!$link){
    header('location:index.php');
    exit();
}

//felhasználok kezelése lesz majd (CRUD)
$action = filter_input(INPUT_GET, 'action');
$tid = filter_input(INPUT_GET, 'tid', FILTER_VALIDATE_INT) ?: null;
$dbTable = 'users';//ebbe a táblába dolgozik a modul
//$output = '';//itt lesznek a kiírandó elemek

if (!empty($_POST)) {
    //var_dump('<pre>',$_POST);
    //hibakezelés
    $hiba = [];//üres hibatömb, ide gyűjtjük a hibákat
    //...hibák kezelése, tárolása
    //Név min 3 karakter
    $name = trim(filter_input(INPUT_POST, 'name'));
    //$name = trim($name);//spacek eltávolítása mindkét oldalról
    $name = strip_tags($name);//html tagek eltávolítása a szövegből... (védelem)
    if (mb_strlen($name, 'utf-8') < 3) {
        $hiba['name'] = '<span class="error">Legalább 3 karakter!</span>';
    }
    //echo $name;

    //EMAIL legyen email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="error">Hibás formátum!</span>';
    } else {
        //email foglaltság ellenőrzése
        $qry = "SELECT id FROM $dbTable WHERE email = '$email' LIMIT 1";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $row = mysqli_fetch_row($result);
        //var_dump($row);
        if ($row !== null && $row[0] != $tid ) {//saját IDnak ne legyen foglalt email hatása
            $hiba['email'] = '<span class="error">Már regisztrált email!</span>';
        }
    }
    //jelszó 1
    $password = filter_input(INPUT_POST, 'password');
    $repassword = filter_input(INPUT_POST, 'repassword');
    if($action == 'create' || $password != '') {//új felvitel esetén, vagy ajelszó 1 mező nem üres
        if (mb_strlen($password, 'utf-8') < 6) {//min 6 karakter
            $hiba['password'] = '<span class="error">Hibás formátum - minimum 6 karakter!</span>';
        } elseif ($password !== $repassword) {//jelszó2
            $hiba['repassword'] = '<span class="error">A jelszavak nem egyeztek!</span>';

        } else {
            //jelszó elkódolása
            $password = password_hash($password, PASSWORD_BCRYPT);
        }
    }
    //Státusz
    $status = filter_input(INPUT_POST, 'status')?:0;


    if (empty($hiba)) {
        //adatok tisztázása
        $data = [
            'name' => $name,
            'email' => $email,
            'status' => $status
        ];

        if($password != ''){//ha van password tegyük az adattömbbe
            $data['password'] = $password;
        }
        if($action == 'create') {
            $data['time_created'] = date('Y-m-d H:i:s');
            //echo '<pre>' . var_export($data, true) . '</pre>';
            //beírás adatbázisba
            $qry = "INSERT INTO 
                        `$dbTable` ( 
                                 `".implode("`,`",array_keys($data))."`) 
                        VALUES ( 
                                '".implode("','",$data)."');";
        }else{
            //módosítás
            $data['time_updated'] = date('Y-m-d H:i:s');
            //UPDATE `users` SET `status` = '0', `time_updated` = '2021-02-13 15:16:44' WHERE `users`.`id` = 1;
            $qry = "UPDATE `$dbTable` SET ";
            $set = [];//SETek tömbje
            foreach($data AS $k => $v){//SET ek
                $set[]= " `$k` = '$v' ";
            }
            $qry .= implode(',',$set);//setek befűzése a querybe
            //záradék
            $qry .= " WHERE id = $tid LIMIT 1";

        }

        mysqli_query($link, $qry) or die(mysqli_error($link));//kérés futtatása
        //átirányítunk a listázásra
        header('location:' . $baseURL);
        exit();
    }
}


switch ($action) {
    case 'delete':
        //ha van id, törlünk
        if ($tid) {
            mysqli_query($link, "DELETE FROM $dbTable WHERE id = $tid LIMIT 1") or die(mysqli_error($link));
        }
        header('location:' . $baseURL);//irányítás a listára
        exit();
        break;

    case 'update':
        $output .= 'módosítunk(id)(kitöltött form ˇˇˇˇ)';
        if($tid){//user adatok lekérése
            $qry = "SELECT name,email,status FROM $dbTable WHERE id = $tid LIMIT 1";
            $result = mysqli_query($link, $qry) or die(mysqli_error($link));
            $row = mysqli_fetch_assoc($result);
            echo '<pre>' . var_export($row, true) . '</pre>';
        }

        if($row){
            //módosítás űrlap
            $form = '<form method="post">
    <h2>Regisztrációs adatlap</h2>';
//név
            $form .= '<label>
        <span>Név<sup>*</sup></span>
        <input type="text" name="name" placeholder="Gipsz Jakab"
               value="' . getValue('name', $row) . '">' . getError('name') . '</label>';

//email
            $form .= '<label>
        <span>Email<sup>*</sup></span>
        <input type="text" name="email" placeholder="email@cim.hu"
               value="' . getValue('email', $row) . '">' . getError('email') . '</label>';

//Jelszó
            $form .= '<label>
        <span>Jelszó<sup>*</sup></span>
        <input type="password" name="password" placeholder="******" value="">' . getError('password') . '</label>';

//Jelszó újra
            $form .= '<label>
        <span>Jelszó újra<sup>*</sup></span>
        <input type="password" name="repassword" placeholder="******" value="">' . getError('repassword') . '</label>';

            //Státusz @todo: HF státusz működés ellenőrzése / megvalósítása (hibás űrlap esetén is teszteld!)
            $checked = filter_input(INPUT_POST, 'status') ? 'checked' : '';//volt-e kipipálva?
            $form .= '<label><span>';
            $form .= '<input type="checkbox" name="status" value="1" ' . $checked . '>
         Státusz</span></label>';
            $form .= '<button>Felhasználó módosítása</button>
        </form>';

            $output .= $form;
        }
        break;

    case 'create':
        $output .= 'új elemet viszünk fel (üres form)';
        //új űrlap
        $form = '<form method="post">
    <h2>Regisztrációs adatlap</h2>';
//név
        $form .= '<label>
        <span>Név<sup>*</sup></span>
        <input type="text" name="name" placeholder="Gipsz Jakab"
               value="' . getValue('name') . '">' . getError('name') . '</label>';

//email
        $form .= '<label>
        <span>Email<sup>*</sup></span>
        <input type="text" name="email" placeholder="email@cim.hu"
               value="' . getValue('email') . '">' . getError('email') . '</label>';

//Jelszó
        $form .= '<label>
        <span>Jelszó<sup>*</sup></span>
        <input type="password" name="password" placeholder="******" value="">' . getError('password') . '</label>';

//Jelszó újra
        $form .= '<label>
        <span>Jelszó újra<sup>*</sup></span>
        <input type="password" name="repassword" placeholder="******" value="">' . getError('repassword') . '</label>';

        //Státusz
        $checked = filter_input(INPUT_POST, 'status') ? 'checked' : '';//volt-e kipipálva?
        $form .= '<label><span>';
        $form .= '<input type="checkbox" name="status" value="1" ' . $checked . '>
         Státusz</span></label>';
        $form .= '<button>Új felhasználó</button>
        </form>';

        $output .= $form;
        break;

    default:
        $table = '';

        //felhasználó lista
        $qry = "SELECT id, name, email, status FROM $dbTable";//lekérés összeösszeállítása
        $results = mysqli_query($link, $qry) or die(mysqli_error($link));
        //új felvitel opció
        $table .= '<a href="'.$baseURL.'&amp;action=create">Új felvitel</a>';
        //táblázat felépítése
        $table .= '<table border="1">';
        //fejléc
        $table .= '<tr>
                        <th>id</th>
                        <th>név</th>
                        <th>email</th>
                        <th>status</th>
                        <th>művelet</th>
                    </tr>';
        //sorok
        while ($row = mysqli_fetch_assoc($results)) {
            $table .= '<tr class="data-row">
                            <td>' . $row["id"] . '</td>
                            <td>' . $row["name"] . '</td>
                            <td>' . $row["email"] . '</td>
                            <td>' . $row["status"] . '</td>
                            <td><a class="btn btn-warning btn-update" href="'.$baseURL.'&amp;action=update&amp;tid=' . $row["id"] . '">módosít</a> | <a onclick="return confirm(\'Biztosan törlöd?\');"  class="btn btn-danger btn-delete" href="'.$baseURL.'&amp;action=delete&amp;tid=' . $row["id"] . '">törlés</a></td>
                        </tr>';
        }
        $table .= '</table>';
        $output .= $table;
        break;
}

//echo $output; //index vette át a kiírását
