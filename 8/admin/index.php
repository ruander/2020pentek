<?php
require "../config/connect.php";//db csatlakozás betöltése
require "../config/settings.php";//beállítások
require "../config/functions.php";//saját eljárások
session_start();
//var_dump($_SESSION, session_id());
//kiléptetünk ha kell
if (filter_input(INPUT_GET, 'logout') !== null) {
    logout();
}


if (!auth()) {
    header('location:login.php');
    exit();
}

//sikeres azonostás
$o = filter_input(INPUT_GET, 'p', FILTER_VALIDATE_INT) ?: 0;//aktív oldal azonositoja

if (!isset(ADMIN_MENU[$o])) $o = 0;//ha hülyeséget kaptunk de egész számot, akkor nem fog létezni az adott index a konstansban, ezért beállítjuk inkább 0 ra (dashboard)
//baseURL - ahova kell itt van előkészítve modulazonosítóval együtt
$baseURL = 'index.php?p='.$o;

$output = '';
//felhasználói rész (név,kilépés)
$userbar = '<div class="userbar">Üdvözöllek <b>' . $_SESSION['userdata']['name'] . '</b>! <a href="?logout">kilépés</a> </div>';
//$output.=$userbar;//outputról most levesszük a userbart, mert más blokkban lesz kiírva

//modul betöltése
$moduleName = ADMIN_MENU[$o]['module'];
$moduleFile = MODULE_DIR . $moduleName . MODULE_EXT;//állandók a setting.php-ból meghatározva
//ha létezik a modul töltsük be
if (file_exists($moduleFile)) {//output is innen lesz
    include $moduleFile;
} else {////ha nincs meg a modul tehát üres marad az output, beletehetjük a hibaüzenetet
    $output = '<div class="error">Nincs ilyen modul:' . $moduleFile . '</div>';
}

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin felület</title>
</head>
<body>
<?php
echo $userbar;//userbar


//admin menü
$menu = '<ul>';

foreach (ADMIN_MENU as $menuID => $menuElem) {//menüpontok a menu konstans bejárásával
    $menu .= '<li><a href="?p=' . $menuID . '" class="' . $menuElem['icon'] . '">' . $menuElem['title'] . '</a></li>';
}

$menu .= '</ul>';

echo $menu;


echo $output;//module output

?>
</body>
</html>





