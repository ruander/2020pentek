<?php
$action = filter_input(INPUT_GET,'action');
/**
 * a switch elágazás segítségével szétválasztjuk a CRUD ágakat
 */
switch($action){
    case 'delete':
        echo 'delete ág';
        break;

    case 'update':
        echo 'módosítunk';
        break;
    case 'create':
        echo 'Új felvitel';
        break;
    default://lista (read)
        echo 'lista';
        //@todo: HF: felhasználó lista
        /**
         * @todo HF irányelvek:
         * 1 lépéses kiírás (switch után 1 echo)
         * Új felvitel link a lista fölé: ?action=create
         * minden sor végén módosít | töröl linkek
         * töröl: ?action=delete&amp;id=userId -> törlünk, ezt: userId
         * módosít: ?action=update&amp;id=userId -> módosítunk, ezt: userId
         * FONTOS: id urlből csak 1 vagy nagyobb EGÉSZ szám lehet
         */
        break;
}