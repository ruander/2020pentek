<?php
//adatbázis csatlakozás a classicmodels adatbázishoz
$dbHost = 'localhost';
$dbUser = 'root';
$dbPass = '';
$dbName = 'php_tanfolyam';

//csatlakozás felépítése (resource)
$link = @mysqli_connect($dbHost,$dbUser,$dbPass,$dbName) or die('Gebasz:'. mysqli_connect_error());
//kódlap illesztése
mysqli_set_charset($link,'utf8');

//Űrlapadatok feldolgozása ha vannak
if (!empty($_POST)) {
    //echo 'POST<pre>' . var_export($_POST, true) . '</pre>';
    //hibakezelés
    $hiba = [];//ide gyűjtjük a hibákat azonos indexre mint ahonnan a postból jönnek

    //NÉV legalább 3 karakter és trim
    $name = trim(filter_input(INPUT_POST, 'name'));//php.net!!!

    if (mb_strlen($name, "utf-8") < 3) {
        $hiba['name'] = '<span class="error">Legalább 3 karakter!</span>';
    }

    //EMAIL legyen email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);

    if (!$email) {
        $hiba['email'] = '<span class="error">Nem érvényes formátum!</span>';
    }else{
        //email foglaltság ellenőrzése
        $qry = "SELECT id FROM users WHERE email = '$email' LIMIT 1";
        $result = mysqli_query($link,$qry) or die(mysqli_error($link));
        $rowUser = mysqli_fetch_row($result);
        if(!empty($rowUser)){
            $hiba['email'] = '<span class="error">Foglalt email cím!</span>';
        }
    }
    //2 jelszó
    //legyen min 6 karakter
    $password = filter_input(INPUT_POST, 'password');
    $repassword = filter_input(INPUT_POST, 'repassword');

    if (mb_strlen($password, 'utf-8') < 6) {//jelszó 1 hossz
        $hiba["password"] = '<span class="error">Minimum 6 karakter!</span>';
    } elseif ($password !== $repassword) {//nem egyezés vizsgálat
        $hiba["repassword"] = '<span class="error">A jelszavak nem egyeztek!</span>';
    }else{
        $password = password_hash($password,PASSWORD_BCRYPT);
    }

    //status előkészítése , de most fixen aktiv, azaz 1 lesz
    //$status = filter_input(INPUT_POST,'status')?:0;
    $status = 1;
    //terms legyen kipipálva
    if (!filter_input(INPUT_POST, 'terms')) {
        $hiba['terms'] = '<span class="error">Kötelező bejelölni!</span>';
    }

    //echo 'POST<pre>' . var_export($hiba, true) . '</pre>';//hibatömb tartalma hibakezelések után
    if (empty($hiba)) {
        //minden adat jó
        //adatok tisztázása
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'status' => $status
        ];
        //új felvitelkor time_created beállítása
        $now = date('Y-m-d H:i:s');
        $data['time_created'] = $now;
        //echo '<pre>' . var_export($data, true) . '</pre>';

        $qry = "INSERT INTO 
`users` (`name`, `email`, `password`, `status`, `time_created`) 
VALUES ('{$data['name']}', '{$data['email']}', '{$data['password']}', '{$data['status']}', '{$data['time_created']}');";
        mysqli_query($link,$qry) or die(mysqli_error($link));


    }

}

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Regisztráció</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        form {
            max-width: 500px;
            margin: 30px auto;
            display: flex;
            flex-flow: column;
            padding: 15px;
        }

        label {
            width: 100%;
            margin: 15px auto 5px;
            display: flex;
            flex-flow: column;
        }

        .error {
            color: #f00;
            font-size: 0.7em;
            font-style: italic;
        }
    </style>
</head>
<body>
<form method="post">
    <h1>Regisztráció</h1>
    <!--név-->
    <label>
        <span>Név<sup>*</sup></span>
        <input type="text" name="name" placeholder="Gipsz Jakab"
               value="<?php echo getValue('name'); ?>">
        <?php
        //mezőhiba kiírása ha van
        echo getError('name');
        ?>
    </label>
    <!--email-->
    <label>
        <span>Email<sup>*</sup></span>
        <input type="text" name="email" placeholder="email@cim.hu"
               value="<?php echo getValue('email'); ?>">
        <?php
        //mezőhiba kiírása ha van
        echo getError('email');
        ?>
    </label>
    <!--jelszó-->
    <label>
        <span>Jelszó<sup>*</sup></span>
        <input type="password" name="password" placeholder="******" value="">
        <?php
        echo getError('password');
        ?>
    </label>
    <!--jelszó mégegyszer-->
    <label>
        <span>Jelszó ismétlése<sup>*</sup></span>
        <input type="password" name="repassword" placeholder="******" value="">
        <?php
        echo getError('repassword');
        ?>
    </label>
    <!--adatvédelmi ny.-->
    <?php
    //ki violt e pipálva küldéskor a tertms mert ha igen, ugy tartjuk
    $checked = filter_input(INPUT_POST, 'terms')?'checked':'';
    ?>
    <label>
        <span><input type="checkbox" name="terms" value="1" <?php echo $checked; ?>> Megértettem az <a href="#" target="_blank">adatvédelmi tájkoztató</a>ban leírtakat!<sup>*</sup></span>
        <?php
        //mezőhiba kiírása ha van
        echo getError('terms');
        ?>
    </label>
    <button>Regisztráció</button>
</form>
</body>
</html>
<?php

/**
 * Hibakiíró eljárás az input mezőkhöz
 * @param $fieldName
 * @return string
 */
function getError($fieldName)
{
    global $hiba;//az eljárás idejéig a hiba változó globális így 'látja' az eljárásunk
    return isset($hiba[$fieldName]) ? $hiba[$fieldName] : '';
}

/**
 * Inputmezők value érték kinyerése a postból
 * @param $fieldName
 * @return string|NULL
 */
function getValue($fieldName){
    return filter_input(INPUT_POST,$fieldName);
}