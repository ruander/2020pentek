<?php

//PHP mysqli: https://www.php.net/manual/en/book.mysqli.php
//adatbázis csatlakozás a classicmodels adatbázishoz
$dbHost = 'localhost';
$dbUser = 'root';
$dbPass = '';
$dbName = 'classicmodels';

//csatlakozás felépítése (resource)
$link = @mysqli_connect($dbHost,$dbUser,$dbPass,$dbName) or die('Gebasz:'. mysqli_connect_error());


//lekérdezés összeállítása
$qry = "SELECT * FROM employees";
$result = mysqli_query($link,$qry) or die(mysqli_error($link));
/*
//egy sor kibontása
$row = mysqli_fetch_row($result);
var_dump($row);

$row = mysqli_fetch_assoc($result);
var_dump($row);

$row = mysqli_fetch_array($result);
var_dump($row);

$row = mysqli_fetch_object($result);
var_dump($row);
//összes kibontása 1 lépésben
$row = mysqli_fetch_all($result,MYSQLI_ASSOC);
var_dump($row);

$row = mysqli_fetch_row($result);
var_dump($row);
*/
$table =  '<table>';//table nyitása
$table .= '<tr>
            <th>Employeenumber</th>
            <th>firstname</th>
            <th>lastname</th>
            <th>extension</th>
            <th>jobtitle</th>
           </tr>';//címsor
//eredmények kibontása ciklusban
while($row = mysqli_fetch_assoc($result)){
    //adatsorok feltöltése
    $table .= '<tr>
                <td>'.$row['employeeNumber'].'</td>
                <td>'.$row['firstName'].'</td>
                <td>'.$row['lastName'].'</td>
                <td>'.$row['extension'].'</td>
                <td>'.$row['jobTitle'].'</td>
               </tr>';//címsor
    //var_dump($row);
}

$table .= '</table>';//table zárása

echo $table;