<!doctype html>
<html lang="hu">
 <head>
  <title>első php fileom</title>
  <meta charset="utf-8">
 </head>
 <body>
 Hello World!<?php
//egy soros php komment
//érdekes video a végtelenről:
//https://www.youtube.com/watch?v=SrU9YDoXE88&t=4s
/*
több
soros
megjegyzés
*/
//kiírás a standard outputra
print "Hello World!";//operátor -> "" :string határoló
echo '<br>Hello ismét<br>';//operátor -> '' :string határoló

//változók - primitivek
$egesz = 5; //integer (int) - egész szám ; operátor '=' -> értékadó operátor
$lebegoPontos = 6/7;//floating point number (float)
$logikai = true;//boolean (bool)
$szoveg = "<h3>A nevem Horváth György</h3>";

//műveletek
$osszeg = $egesz + $lebegoPontos;
$kulonbseg = $egesz - $lebegoPontos;
$szorzat = $egesz * $lebegoPontos;
$hanyad = $egesz / $lebegoPontos; //0 val TILOS osztani!!!

//változók információinak kiírtása (csak fejlesztés alatt!)
var_dump($osszeg,$szoveg,$logikai);
?>
 </body>
</html>