<?php
//tömb megadása alap értékekkel
$tomb = [1, true, 'hello', 6/7];//régebben array(1,true,'hello',6/7)

//objektum megadása
$objektum = (object) $tomb;//tipuskényszerítés objektummá és a kapott érték eltárolása egy változóba (stdClass)

//formázottan írjuk ki a változók értékét és tipusát
var_dump('<pre>',$tomb,$objektum, '</pre>');
//formázottan írjuk ki a változók értékét
echo '<pre>';
echo var_export($tomb, true);
echo '</pre>';

//konkatenáció
$a = "hello";
$b = "world";

echo $a.' '.$b;//helloworld | operátor . -> stringgé konvertálja és összefűzi a 2 oldalán található értékeket

echo '<pre>'.var_export($tomb, true).'</pre>';
echo '<pre>'.var_export($objektum, true).'</pre>';

/*
Elágazás
if(feltétel){
	//igaz ág
}else{
	//hamis ág
}
*/
$veletlenSzam = rand(1,10);
//kiírjuk a generált számot és ami mindig kell
echo "A generált szám: $veletlenSzam, ami ";
//írjuk ki hogy A GENERÁLT 1-10 KÖZÖTTI EGÉSZ SZÁM PÁROS-E
if($veletlenSzam%2 == 0){
	echo "páros.";
}else{
	//páratlan
	echo "páratlan.";
	//echo "A generált\" szám:\$veletlenSzam, ami páratlan.";//operátor \ -> escape, kilépteti az utána köv karaktert a nyelvi elemek közül
}
//shorten if - később
//echo "<br>A generált szám: $veletlenSzam, ami ".($veletlenSzam%2 == 0?'páros':'páratlan');

//for ciklus 
/*
for(ciklusváltozó kezdeti éstéke;belépési feltétel vizsgálata;ciklusváltozó léptetése){
	ciklusmag
}
*/
for($i=1;$i<=20;$i++){
	echo "<br>$i";//ciklusváltozó aktuális értékének kiírása
}
echo "<h1>$i</h1>";//21
