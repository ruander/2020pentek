<?php
//pure PHP file, nem kell php zárótag
$het_napjai = [
    0 => 'Hétfő',
    2 => 'Szerda',
    5 => 'Szombat'
];
var_dump($het_napjai);

$js = "
<script>
    let het_napjai =  [];
    het_napjai[0] = 'Hétfő';
    het_napjai[2] = 'Szerda';
    het_napjai[5] = 'Szombat';
    console.log(het_napjai);
</script>
";
echo $js;//script kiírása egy lépésben
//
$het_napjai = [
    'monday' => 'Hétfő',
    'tuesday' => 'Kedd',
    'wednesday' => 'Szerda',
    'thursday' => 'Csütörtök',
    'friday' => 'Péntek',
    'saturday' => 'Szombat',
    'sunday' => 'Vasárnap'
];
//tömb bejárása
/*
 foreach(halmaz AS $key => $value){
        //ciklusmag $key és value elérhető és az aktuális elem indexe és értéke
}
 * */
foreach ($het_napjai as $k => $v) {
    //var_dump($k,$v);
    echo "<br>Az aktuális kulcs(index): $k, aktuális érték: $v";
}
//több dimenziós tömb
$data = [
    'user' => [
        'id' => '5',
        'name' => 'HGy',
        'verified' => '1',
        'is_admin' => false,
        'email' => 'hgy@ruander.hu'
    ]
];
var_dump('<pre>', $data);
$menu = [
    0 => [
        'title' => 'home',
        'icon' => 'fa-home',
    ],
    1 => [
        'title' => 'about',
        'icon' => 'fa-user',
    ],
    2 => [
        'title' => 'blog',
        'icon' => 'fa-newspaper',
    ],
    3 => [
        'title' => 'contact',
        'icon' => 'fa-envelope',
    ],
];

//a html menuelemeket összegyűjtjük egy változóba amit mindig tovább bővítünk (konkatenáció)
$output = '';
$output .= '<nav>';//nav elem nyitása
$output .= '<ul>';//ul nyitása
//menüpontok listába fűzése
foreach($menu as $menuID => $menuEl){
    $output .= '<li><a href="#'.$menuEl['title'].'" class="fa '.$menuEl['icon'].'">'.$menuEl['title'].'</a></li>';
}
//zárások
$output .= '</ul>';
$output .= '</nav>';

//kiírás egy lépésben
echo $output;

