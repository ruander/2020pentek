<?php
//állandó
const ALLANDO_NEVE = 'Testing...';

var_dump(ALLANDO_NEVE);
//ALLANDO_NEVE = 'Új érték';//állandó nem változhat a futás során

//lehet nem primitív értéke is
const HET_NAPJAI = [
  'Hétfő','Kedd','Szerda','Csütörtök','Péntek','Szombat','Vasárnap'
];
var_dump(HET_NAPJAI[3]);
echo '<p>Teszt: '.ALLANDO_NEVE.'</p>';
echo "<p>Tömbteszt: ".HET_NAPJAI[2]."</p>";

//ciklusok 2
/*
while(belépési feltétel){
    ciklusmag
}
hátultesztelő változat, ez mindenképp lefut legalább 1x
do{
ciklusmag
}while(belépési feltétel)
 */
//előző alkalom for ciklusa while-al
$i = 1;
while($i<=20){
    echo "<br>$i";
    //ciklusváltozó léptetése
    $i++;
}

//véletlen szám előállítása
$veletlenSzam = rand(1,100);
echo "<br>$veletlenSzam";

//elágazás ha páros akkor deeppink felirattal, ha ptlan akkor kék felirattal írjuk ki
if($veletlenSzam%2==0){
    //páros
    $color="deeppink";
}else{
    //ptlan
    $color = "blue";
}

echo "<p style='color:$color'>$veletlenSzam</p>";

//ugyanezt csináljuk meg de úgy hogy biztosan az else ágban kössünk ki
while($veletlenSzam%2==0){//addig generálunk amig a belépési feltétel igaz azaz páros számot kaptunk
    $veletlenSzam = rand(1,100);
}
//
if($veletlenSzam%2==0){
    //páros
    $color="deeppink";
}else{
    //ptlan
    $color = "blue";
}
echo "<p style='color:$color'>$veletlenSzam</p>";

//egyanez for-al
$veletlenSzam = rand(1,100);
for(;$veletlenSzam%2==0;){
    $veletlenSzam = rand(1,100);
}
if($veletlenSzam%2==0){
    //páros
    $color="deeppink";
}else{
    //ptlan
    $color = "blue";
}

echo "<p style='color:$color'>$veletlenSzam</p>";

$limit = 100000000;
//a 2 fajta ciklus összehasonlítása méréssel
$start = microtime(true);
//var_dump($start);
for($i=1;$i<$limit;$i++){
    //üres ciklusmag
}
$stop = microtime(true);
$futasi_ido = $stop-$start;
echo "<p>Ez a foros cucc eddig futott: $futasi_ido</p>";

$start = microtime(true);
//var_dump($start);
$i=1;
while($i<$limit){
    $i++;
    //üres ciklusmag
}
$stop = microtime(true);
$futasi_ido = $stop-$start;
echo "<p>Ez a while cucc eddig futott: $futasi_ido</p>";

//tömb brejárása (js minta for ciklussal) - phpban nem az igazi így
$tombhossz = count(HET_NAPJAI);//7

for($i=0;$i<$tombhossz;$i++){
    echo "<br>".HET_NAPJAI[$i];
}



