<?php
//lotto játék szelvény feladás
$huzasok_szama = 5;
$limit = 90;

//ha kapunk űrlap elemeket, kezeljük őket
if (!empty($_POST)) {
    $hiba = [];
    echo '<pre>' . var_export($_POST, true) . '</pre>';
    //NÉV legalább 3 karakter és trim
    $name = trim(filter_input(INPUT_POST, 'name'));
    if (mb_strlen($name, "utf-8") < 3) {
        $hiba['name'] = '<span class="error">Legalább 3 karakter!</span>';
    }
    //email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="error">Hibás formátum!</span>';
    }
    //tippek
    $tippek = filter_input(INPUT_POST, 'tippek', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    if (!is_array($tippek)) {//valami nagy hiba van.... tesztelheted elemszámra is ha megvan, ellenőrizni mindig lehet többet

    }else{//megvan a tippek tömb
        echo '<pre>' . var_export($tippek, true) . '</pre>';
        $egyedi_tippek = array_unique($tippek);
        echo '<pre>' . var_export($egyedi_tippek, true) . '</pre>';

        //bejárjuk a trippek tömböt és filter_var-ral teszteljük az értékeket
        //szűrő opciók
        $options = [
          'options' => [
              'min_range' => 1,
              'max_range' => $limit
          ]
        ];
        foreach ($tippek as $k => $tipp){
            $test = filter_var($tipp,FILTER_VALIDATE_INT, $options);
            if($test === false){
                $hiba['tippek'][$k] = '<span class="error">Hibás formátum!</span>';
            }elseif( !array_key_exists($k,$egyedi_tippek) ){//tipp rendben, ismélődés kiszűrése
                $hiba['tippek'][$k] = '<span class="error">Ismétlődő tipp!</span>';
            }


            echo "<br>".var_export($test,true);
        }

    }

    if (empty($hiba)) {
        die('minden oké');
        //adatok rendezése

    }
}

//űrlap összeállítása változóba
$form = '<form method="post">';//űrlap nyitás

//név mező beillesztése
$form .= '<label style="display:block">
               Név <input type="text" value="' . getValue('name') . '" name="name" placeholder="Gipsz Jakab">';
//hiba befűzése a mezőbe
$form .= isset($hiba['name']) ? $hiba['name'] : '';
$form .= '</label>';
//email mező beillesztése
$form .= '<label style="display:block">
               Email <input type="text" value="' . getValue('email') . '" name="email" placeholder="you@example.com">';
//hiba befűzése a mezőbe
$form .= isset($hiba['email']) ? $hiba['email'] : '';
$form .= '</label>';
//tippek
for ($i = 1; $i <= $huzasok_szama; $i++) {
    //input elemek elkészítése a tippeknek
    $form .= '<label style="display:block">
               Tipp <input maxlength="5" size="2" type="text" value="' . getValue('tippek',$i) . '" name="tippek[' . $i . ']" placeholder="1-' . $limit . '">';
    //hiba befűzése a mezőbe
    $form .= isset($hiba['tippek'][$i])? $hiba['tippek'][$i] : '';
    $form .= '</label>';//mezőcimke lezárása
}


$form .= '<button>Tippek beküldése</button>
         </form>';//űrlap zárás
//kiírás egy lépésben
echo $form;

/**
 * Hibakiíró eljárás az input mezőkhöz
 * @param $fieldName
 * @return string
 */
function getError($fieldName)
{
    global $hiba;//az eljárás idejéig a hiba változó globális így 'látja' az eljárásunk
    return isset($hiba[$fieldName]) ? $hiba[$fieldName] : '';
}

/**
 * Inputmezők value érték kinyerése a postból
 * @param $fieldName
 * @return string|NULL
 */
function getValue($fieldName, $subKey = false)
{
    if($subKey !== false){//kaptunk 2. paramétert
        $elem = filter_input(INPUT_POST, $fieldName, FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        if(isset($elem[$subKey])){
            return $elem[$subKey];
        }
    }

    return filter_input(INPUT_POST, $fieldName);
}