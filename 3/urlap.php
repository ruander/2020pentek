<?php

//Űrlapadatok feldolgozása ha vannak
if (!empty($_POST)) {
    //A $_POST szuperglobális tömb
    //echo 'POST<pre>' . var_export($_POST, true) . '</pre>';
    //hibakezelés
    $hiba = [];//ide gyűjtjük a hibákat azonos indexre mint ahonnan a postból jönnek
    //echo $_POST['name'];//így sose vedd ki az adatot!!!! mindig filter inputot használj!!!
    //ÍGY˘˘˘˘˘˘--->
    //NÉV legalább 3 karakter és trim
    $name = trim(filter_input(INPUT_POST, 'name'));//php.net!!!
    //$name = trim($name);//ltrim , rtrim //space (és egyéb karakterek) eltávolítása - php.net
    //echo $name;
    if (mb_strlen($name, "utf-8") < 3) {
        $hiba['name'] = '<span class="error">Legalább 3 karakter!</span>';
    }
    //EMAIL legyen email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    //var_dump($email);
    if (!$email) {
        $hiba['email'] = '<span class="error">Nem érvényes formátum!</span>';
    }
    //2 jelszó HF
    //legyen min 6 karakter
    $password = filter_input(INPUT_POST, 'password');
    $repassword = filter_input(INPUT_POST, 'repassword');

    if (mb_strlen($password, 'utf-8') < 6) {//jelszó 1 hossz
        $hiba["password"] = '<span class="error">Minimum 6 karakter!</span>';
    } elseif ($password !== $repassword) {//nem egyezés vizsgálat
        $hiba["repassword"] = '<span class="error">A jelszavak nem egyeztek!</span>';
    }else{
        //$password = md5($password.'poqaeiwzfopdvhfwasp');
        $password = password_hash($password,PASSWORD_BCRYPT);
    }


    //terms legyen kipipálva
    if (!filter_input(INPUT_POST, 'terms')) {
        $hiba['terms'] = '<span class="error">Kötelező bejelölni!</span>';
    }

    //echo 'POST<pre>' . var_export($hiba, true) . '</pre>';//hibatömb tartalma hibakezelések után
    if (empty($hiba)) {
        //minden adat jó
        //adatok tisztázása
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => $password
        ];
        echo '<pre>' . var_export($data, true) . '</pre>';

        //írjuk ki az adatokat a 4es ora adatok nevű mappájába user.txt file-ba (4/adatok/user.txt)
        $dir = '../4/adatok/';
        //mappa létének ellenőrzése (directory type)
        if(!is_dir($dir)){
            //echo 'nem létezik:'.$dir;
            mkdir($dir,0755, true);//létrehozzuk
        }
        $fileName = 'users.json';//ez lesz a file neve
        //HF fopen,fread,fwrite,fclose
        //ha már van ilyen file, olvassuk be a tartalmát és alakítsuk tömbbé
        if(is_file($dir.$fileName)) {
            $fileContent = file_get_contents($dir . $fileName);//tartalom memóriába
            $users = json_decode($fileContent,true);
        }else{
            $users = [];//nincs még file sem ezért tartalma sem lehet ilyenkor az első elem kerül majd a tömbbe
        }
        $users[]=$data;//mostani űrlapelemek hozzádaása az addigi elemekhez
        //$stringSerial = serialize($data);
        //file_put_contents($dir.$fileName,$data);//az adatok nem priomitivek, a fileput contents struktúrálatlanul de hiba nélkül írja a fileba stringként az elemeket
        //file_put_contents($dir.$fileName,$stringSerial);//sorozat... visszaalkítható:unserialize()
        $jsonData = json_encode($users);
        file_put_contents($dir.$fileName,$jsonData);
        echo '<pre>';
        //visszaalakítások
        //var_dump(unserialize($stringSerial));
        var_dump(json_decode($jsonData,true));
        echo '</pre>';
    }


}

?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Regisztráció</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        form {
            max-width: 500px;
            margin: 30px auto;
            display: flex;
            flex-flow: column;
            padding: 15px;
        }

        label {
            width: 100%;
            margin: 15px auto 5px;
            display: flex;
            flex-flow: column;
        }

        .error {
            color: #f00;
            font-size: 0.7em;
            font-style: italic;
        }
    </style>
</head>
<body>
<form method="post">
    <h1>Regisztráció</h1>
    <!--név-->
    <label>
        <span>Név<sup>*</sup></span>
        <input type="text" name="name" placeholder="Gipsz Jakab"
               value="<?php echo getValue('name'); ?>">
        <?php
        //mezőhiba kiírása ha van
        echo getError('name');
        ?>
    </label>
    <!--email-->
    <label>
        <span>Email<sup>*</sup></span>
        <input type="text" name="email" placeholder="email@cim.hu"
               value="<?php echo getValue('email'); ?>">
        <?php
        //mezőhiba kiírása ha van
        echo getError('email');
        ?>
    </label>
    <!--jelszó-->
    <label>
        <span>Jelszó<sup>*</sup></span>
        <input type="password" name="password" placeholder="******" value="">
        <?php
        echo getError('password');
        ?>
    </label>
    <!--jelszó mégegyszer-->
    <label>
        <span>Jelszó ismétlése<sup>*</sup></span>
        <input type="password" name="repassword" placeholder="******" value="">
        <?php
        echo getError('repassword');
        ?>
    </label>
    <!--adatvédelmi ny.-->
    <?php
    //ki violt e pipálva küldéskor a tertms mert ha igen, ugy tartjuk
    $checked = filter_input(INPUT_POST, 'terms')?'checked':'';
    ?>
    <label>
        <span><input type="checkbox" name="terms" value="1" <?php echo $checked; ?>> Megértettem az <a href="#" target="_blank">adatvédelmi tájkoztató</a>ban leírtakat!<sup>*</sup></span>
        <?php
        //mezőhiba kiírása ha van
        echo getError('terms');
        ?>
    </label>
    <button>Regisztráció</button>
</form>
</body>
</html>
<?php

/**
 * Hibakiíró eljárás az input mezőkhöz
 * @param $fieldName
 * @return string
 */
function getError($fieldName)
{
    global $hiba;//az eljárás idejéig a hiba változó globális így 'látja' az eljárásunk
    return isset($hiba[$fieldName]) ? $hiba[$fieldName] : '';
}

/**
 * Inputmezők value érték kinyerése a postból
 * @param $fieldName
 * @return string|NULL
 */
function getValue($fieldName){
    return filter_input(INPUT_POST,$fieldName);
}