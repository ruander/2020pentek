<?php
//2. Készítsünk programot, amely kiszámolja az első 100 darab pozitív egész szám összegét, majd kiírja az eredményt. (Az összeg kiszámolásához vezessünk be egy változót, amelyet a ciklus előtt kinullázunk, a ciklusmagban pedig mindig hozzáadjuk a ciklusváltozó értékét, tehát sorban az 1, 2, 3, 4, ..., 100 számokat.)
$sum = 0;//ez lesz a 'növekmény'
for ($i = 1; $i <= 100; $i++) {
    //ciklusmagban pedig mindig hozzáadjuk a ciklusváltozó értékét
    $sum += $i;//$sum = $sum + $i
}

echo "<div>Az első 100 darab pozitív egész szám összege: $sum</div>";

//21b.Írjon egy ciklust, amelyben egy 10 elemü tömböt véletlen egész értékekkel tölt fel, majd kiírja a tömb legkisebb és legnagyobb elemét.
$tomb = [];//ide gyüjtjük a számokat
while (count($tomb) < 10) {
    //generáljunk egy véletlenszámot a tömb egy új elemének
    $tomb[] = rand(1, 1000);
}
echo '<pre>' . var_export($tomb, true) . '</pre>';
$min = min($tomb);
$max = max($tomb);
echo "<div>Min: $min | Max: $max</div>";

//12.Írjon egy php ciklust, amely előállít egy X*Y cellát tartalmazó táblázatot és mindegyik cellában a Ruander szót helyezi el.
//EGYMÁSBA ÁGYAZOTT CIKLUSOK
$x = rand(3, 8);//sort ennyit szeretnénk
$y = rand(5, 8);//ennyi oszlopot szeretnénk (cellát egy sorban)

$table = '<table border="1">';//ezt a stringet építjük fel, előszőr a tbale tag lesz benne
//ciklus a soroknak
for($i=1;$i<=$x;$i++) {
    //sor nyitása
    $table .= '<tr>';

    //ciklus a celláknak (nem egyezthet a ciklusváltozó neve, ezért itt j-t használunk)
    for ($j = 1; $j <= $y; $j++) {
        //cella
        $table .= '<td>Ruander</td>';
    }
    //sor zárása
    $table .= '</tr>';
}//sorok ciklusa vége
$table .= '</table>';
echo $table;//kiírás egy lépésben

//Órai gyakorlás feladatgyujtemeny.pdf 1-7
//php-gyakorlás, a hf-ből megmaradt feladatok - összes
