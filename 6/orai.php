<?php
//Adatbázis csatlakozás kiépítése
//adatok
$dbHost = 'localhost';
$dbUser = 'root';//xampp esetében
$dbPass = '';
$dbName = 'classicmodels';

//felépítés
$link = @mysqli_connect($dbHost, $dbUser, $dbPass, $dbName) or die(mysqli_connect_error());

//órai feladatok
echo "<h3>10. legelső megrendelés dátuma, ki rendelte?</h3>";
echo $qry = "SELECT 
            orderdate dátum,
            customers.customernumber,
            customername
        FROM
            orders,customers
        WHERE 
            orders.customernumber = customers.customernumber
        ORDER BY orderdate
        LIMIT 1";//lekérdezés összeállítása
//lekérés
$result = mysqli_query($link, $qry) or die(mysqli_error($link));
//eredmény(ek) kibontása
$row = mysqli_fetch_row($result);//array() ->0 indextől
//$row = mysqli_fetch_assoc($result);asszociatív tömb
//$row = mysqli_fetch_object($result);
//$row = mysqli_fetch_array($result);

//válasz kiírása
$output = "<p>A legelső megrendelés dátuma: <b>{$row[0]}</b>, rendelte: <b>{$row[2]} [{$row[1]}]</b></p>";
echo $output;

//5. irodánként mennyit (összeg) rendeltek?

$qry = "SELECT
            of.officecode,
            of.city,
            SUM(priceeach*quantityordered) osszeg
            FROM offices of
            LEFT JOIN employees e
            ON e.officecode = of.officecode
            LEFT JOIN customers c 
            ON salesrepemployeenumber = employeenumber
            LEFT JOIN orders o
            ON o.customernumber = c.customernumber
            LEFT JOIN orderdetails od
            ON od.ordernumber = o.ordernumber
            GROUP BY of.officecode
            ORDER BY osszeg DESC";
$result = mysqli_query($link, $qry) or die(mysqli_error($link));
var_dump(mysqli_num_rows($result));//sorok (recordok) száma

$output = '<table>';//table nyitás
$output .= '<tr>
              <th>irodakód</th>
              <th>város</th>
              <th>összeg</th>
            </tr>';// és címsor
while($row = mysqli_fetch_assoc($result)){//amig tudunk kibontani
    //echo '<pre>'.var_export($row,true).'</pre>';
    $output .= '<tr>
              <td>'.$row['officecode'].'</td>
              <td>'.$row['city'].'</td>
              <td>USD '.$row['osszeg'].'</td>
            </tr>';// adatsor

}
$output .= '</table>';
echo $output;
var_dump(mysqli_fetch_assoc($result));

//olvasnivaló (adattipusok) elsősorban string és numeric
//https://dev.mysql.com/doc/refman/5.7/en/data-types.html
